## Challenge

We hid a message inside of a [Vert.x](http://vertx.io/) verticle "l.o.t.r.Smeagol". This verticle is located inside a [Docker](https://www.docker.com/) image located on Docker Hub at "nineconnections/challenge". This docker image extends from the Vert.x docker image. The verticle is already included in the Vert.x classpath of the docker image. 
When you run the verticle it will listen on the eventbus address "my.precious.is.lost".

Try to find a way to get the message out.