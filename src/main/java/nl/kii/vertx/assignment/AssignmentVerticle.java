package nl.kii.vertx.assignment;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

public class AssignmentVerticle extends AbstractVerticle {

  @Override
  public void start() {
    vertx.createHttpServer()
	.requestHandler(req -> req.response().end("Nine Connections"))
	.listen(8080);
  }
}
